package com.ash.reggie.controller;

import com.ash.reggie.common.R;
import com.ash.reggie.entity.Orders;
import com.ash.reggie.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    // 用户下单
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders) {

        return R.success("下单成功");
    }
}
