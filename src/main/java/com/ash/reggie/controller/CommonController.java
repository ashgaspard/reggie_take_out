package com.ash.reggie.controller;

import com.ash.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/*
 * 文件上传下载管理
 */

@Slf4j
@RestController
@RequestMapping("/common")
public class CommonController {

    @Value("${reggie_pictures.path}")
    private String basePath;

    // 文件上传
    @PostMapping("/upload")
    public R<String> upload(MultipartFile file) throws IOException {
        // file是一个临时文件 需先转存至指定位置 否则本次请求完成后临时文件会删除

        // 原始文件名
        String originalFilename = file.getOriginalFilename();
        // 获取文件后缀
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));

        // 用UUID重新生成文件名 以防止名称重复 造成文件覆盖
        String fileName = UUID.randomUUID().toString() + suffix;

        // 判断 创建目录对象
        File dir = new File(basePath);
        if (!dir.exists()) dir.mkdirs();

        // 将临时文件转存
        file.transferTo(new File(basePath + fileName));
        return R.success(fileName);
    }

    @GetMapping("/download")
    public void downLoad(String name, HttpServletResponse response) throws IOException {
        // 输入流 读取文件
        FileInputStream fis=new FileInputStream(new File(basePath+name));

        // 输出流 写回文件
        ServletOutputStream os = response.getOutputStream();

        response.setContentType("image/jpeg");

        byte[] bytes = new byte[1024];
        int len;
        while ((len=fis.read(bytes))!=-1) os.write(bytes,0,len);
        os.flush();

        // 释放资源
        os.close();
        fis.close();

    }


}
