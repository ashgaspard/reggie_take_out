package com.ash.reggie.controller;

import com.ash.reggie.common.R;
import com.ash.reggie.entity.User;
import com.ash.reggie.service.UserService;
import com.ash.reggie.utils.SMSUtils;
import com.ash.reggie.utils.ValidateCodeUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    // 发送手机短信验证码
    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session) {
        // 获取手机号
        String phone = user.getPhone();
        if (StringUtils.isNotEmpty(phone)) {
            // 生成随机验证码
            String code = ValidateCodeUtils.generateValidateCode(6).toString();
            log.info("code={}", code);
            // 调用阿里云短信服务API发送短信
//            SMSUtils.sendMessage("短信签署名","短信模板名",phone,code);
            // 将验证码保存至session 方便比对校验
            session.setAttribute(phone, code);
            return R.success("短信验证码已发送");
        }
        return R.error("验证码发送失败");
    }

    // 移动端用户登录
    @PostMapping("/login")
    public R<User> login(@RequestBody Map map, HttpSession session) {
        // 获取页面手机号
        String phone = map.get("phone").toString();
        // 获取页面验证码
        String code = map.get("code").toString();
        // 获取session中的验证码
        Object codeInSession = session.getAttribute(phone);
        // 验证码校验
        if (codeInSession != null && codeInSession.equals(code)) {
            // 判断用户是否为新用户
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone, phone);
            User user = userService.getOne(queryWrapper);
            if (user == null) {
                // 若为新用户 自动注册
                user = new User();
                user.setPhone(phone);
//                user.setStatus(1);
                userService.save(user);
            }
            // 将用户id传给session 用于过滤器校验
            session.setAttribute("user", user.getId());
            return R.success(user);
        }
        return R.error("登录失败");
    }
}
