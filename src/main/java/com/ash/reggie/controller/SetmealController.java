package com.ash.reggie.controller;


import com.ash.reggie.common.R;
import com.ash.reggie.dto.SetmealDto;
import com.ash.reggie.entity.Category;
import com.ash.reggie.entity.Setmeal;
import com.ash.reggie.service.CategoryService;
import com.ash.reggie.service.SetmealDishService;
import com.ash.reggie.service.SetmealSeervice;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SetmealSeervice setmealSeervice;

    @Autowired
    private SetmealDishService setmealDishService;


    // 新增套餐
    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto) {
        setmealSeervice.saveWithDish(setmealDto);
        return R.success("新增套餐成功");
    }

    // 套餐的分页查询
    @GetMapping("/page")
    public R<Page> page(int page, int pageSize, String name) {
        // 分页构造器对象
        Page<Setmeal> pageInfo = new Page<>(page, pageSize);
        Page<SetmealDto> dtoPage = new Page<>();

        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        // 根据name模糊查询
        queryWrapper.like(name != null, Setmeal::getName, name);
        // 排序
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        setmealSeervice.page(pageInfo, queryWrapper);

        // 对象拷贝
        BeanUtils.copyProperties(pageInfo, dtoPage, "records");
        List<Setmeal> records = pageInfo.getRecords();

        List<SetmealDto> list = records.stream().map((item) -> {
            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(item, setmealDto);
            Category category = categoryService.getById(item.getCategoryId());
            if (category != null) {
                String categoryName = category.getName();
                setmealDto.setCategoryName(categoryName);
            }
            return setmealDto;
        }).collect(Collectors.toList());

        dtoPage.setRecords(list);
        return R.success(dtoPage);
    }

    // 删除套餐
    @DeleteMapping
    public R<String> delete(@RequestParam List<Long> ids) {
        setmealSeervice.removeWithDish(ids);
        return R.success("套餐删除成功");
    }

    // 根据条件查询套餐数据
    @GetMapping("/list")
    public R<List<Setmeal>> list(Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null, Setmeal::getCategoryId, setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null, Setmeal::getStatus, setmeal.getStatus());

        List<Setmeal> list = setmealSeervice.list(queryWrapper);
        return R.success(list);
    }

}
