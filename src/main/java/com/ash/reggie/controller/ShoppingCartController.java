package com.ash.reggie.controller;

import com.ash.reggie.common.BaseContext;
import com.ash.reggie.common.R;
import com.ash.reggie.entity.ShoppingCart;
import com.ash.reggie.service.ShoppingCartService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    // 将菜品/套餐添加至购物车
    @PostMapping("/add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart) {
        // 获取用户id 以指定此用户对应的购物车
        Long id = BaseContext.getCurrentId();
        shoppingCart.setUserId(id);
        Long dishId = shoppingCart.getDishId();
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, id);
        // 判断当前操作添加的是菜品还是套餐
        if (dishId != null) queryWrapper.eq(ShoppingCart::getDishId, dishId);
        else queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        // 查询此菜品/套餐是否已在购物车中
        // SQL:select * from shopping_cart where user_id = ? and dish_id/setmeal_id = ?
        ShoppingCart one = shoppingCartService.getOne(queryWrapper);

        if (one != null) {
            // 若已存在 则数量加一
            one.setNumber(one.getNumber() + 1);
            shoppingCartService.updateById(one);
        } else {
            // 若不存在 则将此菜品/套餐加入购物车 数量默认为一
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartService.save(shoppingCart);
            one = shoppingCart;
        }
        return R.success(one);
    }

    // 查看购物车
    @GetMapping("/list")
    public R<List<ShoppingCart>> list() {
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        queryWrapper.orderByAsc(ShoppingCart::getCreateTime);

        return R.success(shoppingCartService.list(queryWrapper));
    }

    // 清空购物车
    @DeleteMapping("/clean")
    public R<String> clean() {
        // SQL:delete from shopping_cart where user_id = ?

        // 不可用removeId 因为此方法得到的是购物车id而非用户id
//        shoppingCartService.removeById(id);

        // 应该对用户id对应的购物车进行清空操作
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        shoppingCartService.remove(queryWrapper);
        return R.success("购物车已清空");
    }

    // 购物车商品数量减一
    @PostMapping("/sub")
    public R<ShoppingCart> sub(@RequestBody ShoppingCart shoppingCart) {
        // 获取用户id 以指定此用户对应的购物车
        Long id = BaseContext.getCurrentId();
        shoppingCart.setUserId(id);
        Long dishId = shoppingCart.getDishId();
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, id);
        // 判断当前操作添加的是菜品还是套餐
        if (dishId != null) queryWrapper.eq(ShoppingCart::getDishId, dishId);
        else queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        // 查询此菜品/套餐是否已在购物车中
        // SQL:select * from shopping_cart where user_id = ? and dish_id/setmeal_id = ?
        ShoppingCart one = shoppingCartService.getOne(queryWrapper);

//        if (one != null && one.getNumber() > 1) {
//            // 若已存在 且其数量大于1 则数量减一
//            one.setNumber(one.getNumber() - 1);
//            shoppingCartService.updateById(one);
//        } else if (one != null && one.getNumber() <= 1) {
//            // 若其数量小于1 从购物车中移除此菜品/套餐
//            shoppingCartService.removeById(one.getId());
//            one = null;
//        }

        if (one != null) {
            // 若已存在 则数量减一
            if (one.getNumber() > 1) {
                one.setNumber(one.getNumber() - 1);
                shoppingCartService.updateById(one);
            } else {
                // 若其数量等于1 从购物车中移除此菜品/套餐
                shoppingCartService.removeById(one.getId());
                one = null;
            }
        }
        // 若此菜品/套餐不在购物车中 不进行操作
        return R.success(one);
    }
}
