package com.ash.reggie.dto;

import com.ash.reggie.entity.Setmeal;
import com.ash.reggie.entity.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
