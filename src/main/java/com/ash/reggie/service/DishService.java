package com.ash.reggie.service;

import com.ash.reggie.dto.DishDto;
import com.ash.reggie.entity.Dish;
import com.baomidou.mybatisplus.extension.service.IService;

public interface DishService extends IService<Dish> {

    // 新增菜品 将数据同时插入菜品表dish 口味表dish_flavor
    public void saveWithFlavor(DishDto dishDto);

    // 根据id查询菜品信息与对应口味
    public DishDto getByIdWithFlavor(Long id);

    // 更新菜品信息 同时更新对应的口味信息
    public void updateWithFlavor(DishDto dishDto);
}
