package com.ash.reggie.service;

import com.ash.reggie.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

public interface OrdersService extends IService<Orders> {

    // 用户下单
    public void submit(Orders orders);
}
