package com.ash.reggie.service;

import com.ash.reggie.entity.Employee;
import com.baomidou.mybatisplus.extension.service.IService;

public interface EmployeeService extends IService<Employee> {

}
