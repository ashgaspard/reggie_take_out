package com.ash.reggie.service;

import com.ash.reggie.dto.SetmealDto;
import com.ash.reggie.entity.Setmeal;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface SetmealSeervice extends IService<Setmeal> {

    // 新增套餐 同时保存套餐与菜品的关联关系
    public void saveWithDish(SetmealDto setmealDto);

    // 删除套餐 同时删除套餐与菜品的关联关系
    public void removeWithDish(List<Long> ids);
}
