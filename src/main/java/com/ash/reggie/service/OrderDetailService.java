package com.ash.reggie.service;

import com.ash.reggie.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

public interface OrderDetailService extends IService<OrderDetail> {

}
