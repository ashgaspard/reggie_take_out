package com.ash.reggie.service;

import com.ash.reggie.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

public interface UserService extends IService<User> {
}
