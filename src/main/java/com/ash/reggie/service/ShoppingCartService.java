package com.ash.reggie.service;

import com.ash.reggie.entity.ShoppingCart;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ShoppingCartService extends IService<ShoppingCart> {

}
