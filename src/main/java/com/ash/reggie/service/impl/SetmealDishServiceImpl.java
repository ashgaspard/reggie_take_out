package com.ash.reggie.service.impl;

import com.ash.reggie.entity.SetmealDish;
import com.ash.reggie.mapper.SetmealDishMapper;
import com.ash.reggie.service.SetmealDishService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish>implements SetmealDishService {

}
