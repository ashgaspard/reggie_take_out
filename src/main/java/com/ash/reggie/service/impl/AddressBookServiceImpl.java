package com.ash.reggie.service.impl;

import com.ash.reggie.entity.AddressBook;
import com.ash.reggie.mapper.AddressBookMapper;
import com.ash.reggie.service.AddressBookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {

}
