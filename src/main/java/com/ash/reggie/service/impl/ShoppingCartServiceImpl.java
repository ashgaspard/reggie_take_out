package com.ash.reggie.service.impl;

import com.ash.reggie.entity.ShoppingCart;
import com.ash.reggie.mapper.ShoppingCartMapper;
import com.ash.reggie.service.ShoppingCartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {

}
