package com.ash.reggie.service.impl;

import com.ash.reggie.common.CustomException;
import com.ash.reggie.dto.SetmealDto;
import com.ash.reggie.entity.Setmeal;
import com.ash.reggie.entity.SetmealDish;
import com.ash.reggie.mapper.SetmealMapper;
import com.ash.reggie.service.SetmealDishService;
import com.ash.reggie.service.SetmealSeervice;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealSeervice {

    @Autowired
    private SetmealDishService setmealDishService;

    // 新增套餐 同时保存套餐与菜品的关联关系
    @Transactional
    public void saveWithDish(SetmealDto setmealDto) {
        // 套餐基本信息
        this.save(setmealDto);

        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes.stream().map((item) -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());

        // 保存关联关系
        setmealDishService.saveBatch(setmealDishes);
    }

    // 删除套餐 同时删除套餐与菜品的关联关系
    @Transactional
    @Override
    public void removeWithDish(List<Long> ids) {
        // 先查询套餐状态 仅停售状态可以被删除
        // 类似于以下sql语句效果
        // select count(*) from setmeal where id in (id1,id2,id3) and status = 1

        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.in(Setmeal::getId, ids);
        queryWrapper.eq(Setmeal::getStatus, 1);

        // 统计处于在售状态的套餐数量
        int count = this.count(queryWrapper);
        // 若不可删除 抛异常
        if (count > 0) throw new CustomException("此套餐正在售卖，不可删除");

        // 若可删除 则应先删除套餐表的数据
        this.removeByIds(ids);

        // 再删除关系表的数据
        // 类似于以下sql语句效果
        // delete from setmeal dish where setmeal_id in (id1,id2,id3)
        LambdaQueryWrapper<SetmealDish> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(SetmealDish::getSetmealId, ids);
        
        setmealDishService.remove(wrapper);
    }
}
