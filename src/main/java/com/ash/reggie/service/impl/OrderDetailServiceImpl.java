package com.ash.reggie.service.impl;

import com.ash.reggie.entity.OrderDetail;
import com.ash.reggie.mapper.OrderDetailMapper;
import com.ash.reggie.service.OrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {

}
