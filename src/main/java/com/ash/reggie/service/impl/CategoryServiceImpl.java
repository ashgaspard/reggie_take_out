package com.ash.reggie.service.impl;

import com.ash.reggie.common.CustomException;
import com.ash.reggie.entity.Category;
import com.ash.reggie.entity.Dish;
import com.ash.reggie.entity.Setmeal;
import com.ash.reggie.mapper.CategoryMapper;
import com.ash.reggie.service.CategoryService;
import com.ash.reggie.service.DishService;
import com.ash.reggie.service.SetmealSeervice;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
    private DishService dishService;

    @Autowired
    private SetmealSeervice setmealSeervice;


    // 根据id删除分类 在删除前先判断
    @Override
    public void remove(Long id) {
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<Dish>();
        // 根据category_id查询
        dishLambdaQueryWrapper.eq(Dish::getCategoryId, id);
        int count1 = dishService.count(dishLambdaQueryWrapper);
        // 查询当前分类是否关联了菜品 若已关联 抛异常不让删
        if (count1 > 0) {
            throw new CustomException("此分类已关联菜品，不可删除");
        }

        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        setmealLambdaQueryWrapper.eq(Setmeal::getCategoryId, id);
        int count2 = setmealSeervice.count(setmealLambdaQueryWrapper);
        // 查询当前分类是否关联了套餐 若已关联 抛异常不让删
        if (count2 > 0) {
            throw new CustomException("此分类已关联套餐，不可删除");
        }

        // 正常删除
        super.removeById(id);
    }
}
