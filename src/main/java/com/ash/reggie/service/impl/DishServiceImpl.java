package com.ash.reggie.service.impl;

import com.ash.reggie.dto.DishDto;
import com.ash.reggie.entity.Dish;
import com.ash.reggie.entity.DishFlavor;
import com.ash.reggie.mapper.DishMapper;
import com.ash.reggie.service.DishFlavorService;
import com.ash.reggie.service.DishService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Autowired
    private DishFlavorService dishFlavorService;

    // 新增菜品 同时保存对应口味数据

    @Transactional
    public void saveWithFlavor(DishDto dishDto) {
        // 先保存菜品基本信息至表dish
        this.save(dishDto);
        // 菜品id
        Long dishId = dishDto.getId();
        // 菜品口味
        List<DishFlavor> flavors = dishDto.getFlavors();

        flavors = flavors.stream().map((item) -> {
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());

        // 再保存菜品口味至dish_flavor
        dishFlavorService.saveBatch(flavors);
    }

    // 根据id查询菜品信息与对应口味
    @Override
    public DishDto getByIdWithFlavor(Long id) {

        // 先查询基本信息 从dish表获取
        Dish dish = this.getById(id);

        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish, dishDto);

        // 再查询口味信息 从dish_flavor获取
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, dish.getId());
        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);
        dishDto.setFlavors(flavors);
        return dishDto;
    }

    @Override
    @Transactional
    public void updateWithFlavor(DishDto dishDto) {
        // 先更新dish表基本信息
        this.updateById(dishDto);

        // 再更新dish_flavor表口味信息 （先清除 再插入）
        // delete
        LambdaQueryWrapper<DishFlavor> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dishDto.getId());
        dishFlavorService.remove(queryWrapper);
        // insert
        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors = flavors.stream().map((item) -> {
            item.setId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());

        dishFlavorService.saveBatch(flavors);
    }

}
