package com.ash.reggie.service.impl;

import com.ash.reggie.entity.Employee;
import com.ash.reggie.mapper.EmployeeMapper;
import com.ash.reggie.service.EmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee>implements EmployeeService {


}
