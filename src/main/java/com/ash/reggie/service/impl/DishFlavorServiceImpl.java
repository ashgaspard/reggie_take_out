package com.ash.reggie.service.impl;


import com.ash.reggie.entity.DishFlavor;
import com.ash.reggie.mapper.DishFlavorMapper;
import com.ash.reggie.service.DishFlavorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor>implements DishFlavorService {

}
