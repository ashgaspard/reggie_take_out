package com.ash.reggie.service.impl;

import com.ash.reggie.entity.User;
import com.ash.reggie.mapper.UserMapper;
import com.ash.reggie.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>implements UserService {

}
