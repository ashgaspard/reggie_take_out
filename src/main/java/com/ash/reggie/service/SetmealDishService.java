package com.ash.reggie.service;

import com.ash.reggie.entity.SetmealDish;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SetmealDishService extends IService<SetmealDish> {
}
